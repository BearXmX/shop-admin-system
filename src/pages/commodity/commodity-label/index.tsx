import ShopEmpty from '@/components/empty'

const CommodityLabel: React.FC = () => {
  return (
    <div className="shop-layout-content">
      {' '}
      <ShopEmpty />
    </div>
  )
}

export default CommodityLabel
