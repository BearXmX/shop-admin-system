import ShopEmpty from '@/components/empty'

const CommodityList: React.FC = () => {
  return (
    <div className="shop-layout-content">
      {' '}
      <ShopEmpty />{' '}
    </div>
  )
}

export default CommodityList
