import ShopEmpty from '@/components/empty'

const CreateCategory: React.FC = () => {
  return (
    <div className="shop-layout-content">
      {' '}
      <ShopEmpty />
    </div>
  )
}

export default CreateCategory
