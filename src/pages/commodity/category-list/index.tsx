/*
 * @Date: 2022-05-02 16:45:13
 * @LastEditors: 熊明祥
 * @LastEditTime: 2022-05-04 17:12:55
 * @Description:
 */
import ShopEmpty from '@/components/empty'
const CategoryList: React.FC = () => {
  return (
    <div className="shop-layout-content">
      <ShopEmpty />
    </div>
  )
}

export default CategoryList
