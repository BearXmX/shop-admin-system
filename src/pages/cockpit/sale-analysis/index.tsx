import ShopEmpty from '@/components/empty'

const SalesAnalysis: React.FC = () => {
  return (
    <div className="shop-layout-content">
      <ShopEmpty />
    </div>
  )
}

export default SalesAnalysis
