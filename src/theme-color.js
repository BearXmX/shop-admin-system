const themeColor = {
  '@primary-color': '#5b6aff',// 全局主色
  '@gray-color': '#9da2ac'
}

module.exports = themeColor
